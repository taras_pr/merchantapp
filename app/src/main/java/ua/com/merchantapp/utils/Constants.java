package ua.com.merchantapp.utils;

import ua.com.merchantapp.models.TransactionDetail;

public class Constants {
    public static final String RATES_ENDPOINT = "/rates.json";
    public static final String TRANSACTIONS_ENDPOINT = "/transactions.json";

    public static final String EXTRA_DETAILS = "EXTRA_DETAILS";
    public static final String EXTRA_VIEW_RV = "EXTRA_VIEW_RV";
    public static final String EXTRA_SKU = "EXTRA_SKU";
    public static final String EXTRA_RATES = "EXTRA_RATES";
    public static final String CURRENCY_GOLD = "Gold";
    public static final int ACCURACY = 5;
}
