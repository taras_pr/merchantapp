package ua.com.merchantapp.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Set;

import ua.com.merchantapp.models.RateDetail;
import ua.com.merchantapp.models.RateResponse;

public class Utils {

    public static void matchRatesMap(HashMap<String, HashMap<String, Double>> ratesMap, RateResponse rateResponse) {
        for (RateDetail rateDetail:rateResponse){
            HashMap<String, Double> detailMap = ratesMap.get(rateDetail.getFrom());
            if (detailMap==null)
                detailMap = new HashMap<String, Double>();

            detailMap.put(rateDetail.getTo(), Double.valueOf(rateDetail.getRate()));

            ratesMap.put(rateDetail.getFrom(), detailMap);
        }
    }

    public static void calculateMissingRates(HashMap<String, HashMap<String, Double>> rateMap) {
        Set<String> currenciesSet = rateMap.keySet();
        for (String currency1 : currenciesSet){
            for (String currency2 : currenciesSet) {
                if (currency1.equals(currency2))
                    continue;

                HashMap<String, Double> rateDetail = rateMap.get(currency1);
                if (rateDetail==null){
                    rateDetail = new HashMap<String, Double>();
                }

                Double rate = getRateByCurrencies(currency1, currency2, rateMap);

                rateDetail.put(currency2, rate);

                rateMap.put(currency1, rateDetail);

            }
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static Double getRateByCurrencies(String currency1, String askedCurrency, HashMap<String, HashMap<String, Double>> ratesMap){

        HashMap<String, Double> availableRates = ratesMap.get(currency1);
        Double rate = availableRates.get(askedCurrency);
        if (rate!=null)
            return rate;
        else
            return getByAvailableEntireCurrencies(currency1, askedCurrency, ratesMap);

    }


    public static Double getByAvailableEntireCurrencies(String currentCurrency, String askedCurrency, HashMap<String, HashMap<String, Double>> ratesMap){

        Double mirrorRate = ratesMap.get(askedCurrency).get(currentCurrency);
        if (mirrorRate!=null){
            return 1/mirrorRate;
        }

        HashMap<String, Double> entireRates = ratesMap.get(currentCurrency);

        Set<String> availableCountrySets = entireRates.keySet();
        for (String availableCurrency:availableCountrySets){
            Double rateByAvailableCurrency = entireRates.get(availableCurrency);
            Double rateByAskedCurrency = getRateByCurrencies(availableCurrency, askedCurrency, ratesMap);
            if (rateByAskedCurrency!=null){
                return (1/(1 / rateByAskedCurrency / rateByAvailableCurrency));
            } else {
                return 0d;
            }
        }

        return 0d;
    }

}
