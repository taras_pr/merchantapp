package ua.com.merchantapp;

import android.app.ActivityOptions;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ua.com.merchantapp.models.TransactionDetail;
import ua.com.merchantapp.utils.Constants;
import ua.com.merchantapp.utils.Utils;

public class SkuDetailsActivity extends BaseActivity {

    String skuString;

    ArrayList<TransactionDetail> detailList;

    RecyclerView transactionsRecyclerView;

    Double total = 0d;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        transactionsRecyclerView = findViewById(R.id.transactionsRecyclerView);

        detailList = getIntent().getParcelableArrayListExtra(Constants.EXTRA_DETAILS);
        skuString = getIntent().getStringExtra(Constants.EXTRA_SKU);
        ratesMap = (HashMap<String, HashMap<String, Double>>) getIntent().getSerializableExtra(Constants.EXTRA_RATES);


        for (TransactionDetail transactionDetail:detailList){
            String currency = transactionDetail.getCurrency();
            if (!currency.equals(Constants.CURRENCY_GOLD)){
                Double rate = ratesMap.get(currency).get(Constants.CURRENCY_GOLD);
                total += Double.valueOf(transactionDetail.getAmount())*rate;
            } else {
                total += Double.valueOf(transactionDetail.getAmount());
            }
        }

        setTitle(skuString);
        getSupportActionBar().setSubtitle(String.valueOf(total));



        transactionsRecyclerView = findViewById(R.id.transactionsRecyclerView);
        transactionsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        transactionsRecyclerView.setAdapter(getTransactionsAdapter().setTransactionDetailsList(detailList));


        ViewCompat.setTransitionName(transactionsRecyclerView, Constants.EXTRA_VIEW_RV);

        getTransactionsAdapter().notifyDataSetChanged();


    }

    @Override
    public void openDetailsActivity(View v, String sku) {}

}
