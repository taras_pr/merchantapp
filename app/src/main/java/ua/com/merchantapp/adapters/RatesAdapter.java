package ua.com.merchantapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ua.com.merchantapp.R;
import ua.com.merchantapp.models.RateDetail;

public class RatesAdapter extends RecyclerView.Adapter<RatesAdapter.RatesViewHolder> {

    Context context;
    ArrayList<RateDetail> rateDetailArrayList;


    public RatesAdapter(Context context, ArrayList<RateDetail> rateDetailArrayList) {
        this.context = context;
        this.rateDetailArrayList = rateDetailArrayList;
    }

    public RatesAdapter setRateDetailArrayList(ArrayList<RateDetail> rateDetailArrayList) {
        this.rateDetailArrayList = rateDetailArrayList;
        return this;
    }

    @NonNull
    @Override
    public RatesAdapter.RatesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rate_view, viewGroup, false);
        return new RatesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RatesAdapter.RatesViewHolder ratesViewHolder, int i) {
        RateDetail rateDetail = rateDetailArrayList.get(i);
        ratesViewHolder.rateTextView.setText(rateDetail.getRate());
        ratesViewHolder.fromTextView.setText(rateDetail.getFrom());
        ratesViewHolder.toTexttView.setText(rateDetail.getTo());

    }

    @Override
    public int getItemCount() {
        return rateDetailArrayList.size();
    }

    public class RatesViewHolder extends RecyclerView.ViewHolder {
        TextView fromTextView, toTexttView, rateTextView;
        public RatesViewHolder(@NonNull View itemView) {
            super(itemView);
            fromTextView = itemView.findViewById(R.id.fromTextView);
            toTexttView = itemView.findViewById(R.id.toTextView);
            rateTextView = itemView.findViewById(R.id.rateTextView);
        }
    }
}
