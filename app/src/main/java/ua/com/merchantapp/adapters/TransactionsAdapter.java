package ua.com.merchantapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ua.com.merchantapp.BaseActivity;
import ua.com.merchantapp.MainActivity;
import ua.com.merchantapp.R;
import ua.com.merchantapp.models.TransactionDetail;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionsViewHolder> {

    Context context;
    ArrayList<TransactionDetail> transactionDetails;
    View.OnClickListener onClickListener;


    public TransactionsAdapter(Context context, ArrayList<TransactionDetail> transactionDetails) {
        this.context = context;
        this.transactionDetails = transactionDetails;
    }

    public TransactionsAdapter setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    public TransactionsAdapter setTransactionDetailsList(ArrayList<TransactionDetail> transactionDetails) {
        this.transactionDetails = transactionDetails;
        return this;
    }

    @NonNull
    @Override
    public TransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_transaction_view, viewGroup, false);
        return new TransactionsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionsViewHolder transactionsViewHolder, int i) {
        final TransactionDetail transactionDetail = transactionDetails.get(i);
        transactionsViewHolder.skuTextView.setText(transactionDetail.getSku());
        transactionsViewHolder.currencyTexttView.setText(transactionDetail.getCurrency());
        transactionsViewHolder.amountTextView.setText(transactionDetail.getAmount());
        transactionsViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity)context).openDetailsActivity(v, transactionDetail.getSku());
            }
        });


    }

    @Override
    public int getItemCount() {
        return transactionDetails.size();
    }

    public class TransactionsViewHolder extends RecyclerView.ViewHolder {
        View rootView;
        TextView skuTextView, currencyTexttView, amountTextView;
        public TransactionsViewHolder(@NonNull View itemView) {
            super(itemView);
            rootView = itemView.findViewById(R.id.rootView);
            skuTextView = itemView.findViewById(R.id.skuTextView);
            currencyTexttView = itemView.findViewById(R.id.currencyTextView);
            amountTextView = itemView.findViewById(R.id.amountTextView);
        }
    }
}
