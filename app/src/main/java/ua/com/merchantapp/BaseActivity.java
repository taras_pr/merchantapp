package ua.com.merchantapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import ua.com.merchantapp.adapters.RatesAdapter;
import ua.com.merchantapp.adapters.TransactionsAdapter;
import ua.com.merchantapp.models.RateDetail;
import ua.com.merchantapp.models.TransactionDetail;

public abstract class BaseActivity extends AppCompatActivity {

    RatesAdapter ratesAdapter;
    ArrayList<RateDetail> rateDetailArrayList;

    protected HashMap<String, HashMap<String, Double>> ratesMap = new HashMap<>();


    TransactionsAdapter transactionsAdapter;
    ArrayList<TransactionDetail> transactionDetailArrayList;

    protected RecyclerView transactionsRecyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public abstract void openDetailsActivity(View v, String sku);

    public ArrayList<RateDetail> getRateDetailArrayList() {
        if (rateDetailArrayList==null)
            rateDetailArrayList = new ArrayList<>();
        return rateDetailArrayList;
    }

    public ArrayList<TransactionDetail> getTransactionDetailArrayList() {
        if (transactionDetailArrayList==null)
            transactionDetailArrayList = new ArrayList<>();
        return transactionDetailArrayList;
    }

    protected RatesAdapter getRatesAdapter() {
        if (ratesAdapter==null){
            ratesAdapter = new RatesAdapter(this, getRateDetailArrayList());
        }

        return ratesAdapter;
    }

    protected TransactionsAdapter getTransactionsAdapter() {
        if (transactionsAdapter==null){
            transactionsAdapter = new TransactionsAdapter(this, getTransactionDetailArrayList());
        }

        return transactionsAdapter;
    }

}
