package ua.com.merchantapp.services;

import retrofit2.Call;
import retrofit2.http.GET;
import ua.com.merchantapp.models.RateResponse;
import ua.com.merchantapp.models.TransactionDetail;
import ua.com.merchantapp.models.TransactionsResponse;
import ua.com.merchantapp.utils.Constants;

public interface TransactionsService {

    @GET(Constants.TRANSACTIONS_ENDPOINT)
    public Call<TransactionsResponse> getTransactions();

}
