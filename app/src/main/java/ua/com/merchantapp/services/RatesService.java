package ua.com.merchantapp.services;

import retrofit2.Call;
import retrofit2.http.GET;
import ua.com.merchantapp.models.RateDetail;
import ua.com.merchantapp.models.RateResponse;
import ua.com.merchantapp.utils.Constants;

public interface RatesService {

    @GET(Constants.RATES_ENDPOINT)
    public Call<RateResponse> getRates();
}
