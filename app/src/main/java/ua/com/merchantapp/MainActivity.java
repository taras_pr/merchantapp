package ua.com.merchantapp;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Pair;
import android.view.View;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import ua.com.merchantapp.loaders.RatesLoader;
import ua.com.merchantapp.loaders.TransactionsLoader;
import ua.com.merchantapp.models.RateResponse;
import ua.com.merchantapp.models.RequestResult;
import ua.com.merchantapp.models.Response;
import ua.com.merchantapp.models.TransactionDetail;
import ua.com.merchantapp.models.TransactionsResponse;
import ua.com.merchantapp.utils.Constants;
import ua.com.merchantapp.utils.Utils;

public class MainActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Response> {

    String ratesListString = "[{ \"from\": \"Bronze\", \"to\": \"Silver\", \"rate\": \"1.37\" }, { \"from\": \"Silver\", \"to\": \"Bronze\", \"rate\": \"0.73\" }, " +
            "{ \"from\": \"Bronze\", \"to\": \"Gold\", \"rate\": \"1.05\" }, " +
            "{ \"from\": \"Gold\", \"to\": \"Bronze\", \"rate\": \"0.95\" }, { \"from\": \"Silver\", \"to\": \"Copper\", \"rate\": \"0.51\" }, { " +
            "\"from\": \"Copper\", \"to\": \"Silver\", \"rate\": \"1.96\" } ]";

    String transactionsListString = "[{\"sku\":\"T1091\",\"amount\":\"24.6\",\"currency\":\"Copper\"},{\"sku\":\"I7897\",\"amount\":\"25.4\",\"currency\":\"" +
            "Silver\"},{\"sku\":\"H2313\",\"amount\":\"34.4\",\"currency\":\"Gold\"}," +
            "{\"sku\":\"R1893\",\"amount\":\"15.5\",\"currency\":\"Silver\"},{\"sku\":\"T1091\",\"amount\":\"22.2\",\"currency\":\"" +
            "Gold\"},{\"sku\":\"I1421\",\"amount\":\"28.2\",\"currency\":\"Silver\"}, " +
            "{\"sku\":\"S9069\",\"amount\":\"22.5\",\"currency\":\"Silver\"},{\"sku\":\"E7084\",\"amount\":\"24.4\",\"currency\":\"B " +
            "ronze\"},{\"sku\":\"T1091\",\"amount\":\"17.3\",\"currency\":\"Bronze\"}]";

    private static final int ID_LOADER_RATES = 1;
    private static final int ID_LOADER_TRANSACTIONS = 2;


    HashMap<String, ArrayList<TransactionDetail>> transactionsHashMap = new HashMap<>();
    Set<String> currenciesSet = new HashSet<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        transactionsRecyclerView = findViewById(R.id.transactionsRecyclerView);
        transactionsRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        transactionsRecyclerView.setAdapter(getTransactionsAdapter());

        LoaderManager.getInstance(this).restartLoader(ID_LOADER_RATES, Bundle.EMPTY, this);
    }



    @NonNull
    @Override
    public Loader<Response> onCreateLoader(int i, @Nullable Bundle bundle) {
        switch (i){
            case ID_LOADER_RATES:
                return new RatesLoader(this);
            case ID_LOADER_TRANSACTIONS:
                return new TransactionsLoader(this);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Response> loader, Response response) {
        int id = loader.getId();
        if (response.getRequestResult().equals(RequestResult.SUCCESS)) {
            switch (id) {
                case ID_LOADER_RATES:


                    RateResponse rateResponse = response.getTypedAnswer();
                    //RateResponse rateResponse = new Gson().fromJson(ratesListString, RateResponse.class);

                    Utils.matchRatesMap(ratesMap, rateResponse);

                    Utils.calculateMissingRates(ratesMap);

                    LoaderManager.getInstance(this).restartLoader(ID_LOADER_TRANSACTIONS, Bundle.EMPTY, this);

                    break;

                case ID_LOADER_TRANSACTIONS:
                    TransactionsResponse transactionsResponse = response.getTypedAnswer();
                    //TransactionsResponse transactionsResponse = new Gson().fromJson(transactionsListString, TransactionsResponse.class);

                    for (TransactionDetail transactionDetail:transactionsResponse){
                        ArrayList<TransactionDetail> mapDetails = transactionsHashMap.get(transactionDetail.getSku());
                        if (mapDetails==null)
                            mapDetails = new ArrayList<>();
                        mapDetails.add(transactionDetail);

                        transactionsHashMap.put(transactionDetail.getSku(), mapDetails);
                    }

                    getTransactionsAdapter()
                            .setTransactionDetailsList(transactionsResponse)
                            .notifyDataSetChanged();

                    break;
            }
        }

        LoaderManager.getInstance(this).destroyLoader(id);

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Response> loader) { }

    public void openDetailsActivity(View v, String sku){
        Intent intent = new Intent(this, SkuDetailsActivity.class);
        intent.putParcelableArrayListExtra(Constants.EXTRA_DETAILS, transactionsHashMap.get(sku));
        intent.putExtra(Constants.EXTRA_SKU, sku);
        intent.putExtra(Constants.EXTRA_RATES, ratesMap);

        ActivityOptions options = ActivityOptions
                .makeSceneTransitionAnimation(this,
                        Pair.create(v, Constants.EXTRA_VIEW_RV));

        startActivity(intent, options.toBundle());
    }


}
