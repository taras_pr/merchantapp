package ua.com.merchantapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionDetail implements Parcelable {
    private String sku;
    private String amount;
    private String currency;

    protected TransactionDetail(Parcel in) {
        sku = in.readString();
        amount = in.readString();
        currency = in.readString();
    }

    public static final Creator<TransactionDetail> CREATOR = new Creator<TransactionDetail>() {
        @Override
        public TransactionDetail createFromParcel(Parcel in) {
            return new TransactionDetail(in);
        }

        @Override
        public TransactionDetail[] newArray(int size) {
            return new TransactionDetail[size];
        }
    };

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sku);
        dest.writeString(amount);
        dest.writeString(currency);
    }
}
