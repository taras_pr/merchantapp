package ua.com.merchantapp.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.io.IOException;

import ua.com.merchantapp.models.RequestResult;
import ua.com.merchantapp.models.Response;


/**
 * Created by taras on 4/18/17.
 */

public abstract class BaseLoader extends AsyncTaskLoader<Response> {

        public BaseLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Response loadInBackground() {
            try {
                Response response = apiCall();
                if (response.getRequestResult() == RequestResult.SUCCESS) {
                    onSuccess();
                } else {
                    onError();
                }
                return response;
            } catch (IOException e) {
                onError();
                return new Response();
            }
        }

        protected void onSuccess() {
        }

        protected void onError() {
        }

        protected abstract Response apiCall() throws IOException;

}
