package ua.com.merchantapp.loaders;

import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ua.com.merchantapp.models.RateResponse;
import ua.com.merchantapp.models.RequestResult;
import ua.com.merchantapp.models.Response;
import ua.com.merchantapp.models.TransactionsResponse;
import ua.com.merchantapp.retrofit.ApiFactory;
import ua.com.merchantapp.services.RatesService;
import ua.com.merchantapp.services.TransactionsService;

public class TransactionsLoader extends BaseLoader {

    public TransactionsLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        TransactionsService transactionsService = ApiFactory.getTransactionsService();
        Call<TransactionsResponse> transactionsResponseCall = transactionsService.getTransactions();
        TransactionsResponse transactionsResponse = transactionsResponseCall.execute().body();
        if (transactionsResponse!=null) {
            return new Response().setAnswer(transactionsResponse).setRequestResult(RequestResult.SUCCESS);
        } else {
            return new Response().setAnswer(transactionsResponse).setRequestResult(RequestResult.ERROR);
        }
    }

}
