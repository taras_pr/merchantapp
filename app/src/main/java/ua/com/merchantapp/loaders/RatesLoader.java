package ua.com.merchantapp.loaders;

import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ua.com.merchantapp.models.RateResponse;
import ua.com.merchantapp.models.RequestResult;
import ua.com.merchantapp.models.Response;
import ua.com.merchantapp.retrofit.ApiFactory;
import ua.com.merchantapp.services.RatesService;

public class RatesLoader extends BaseLoader {

    public RatesLoader(Context context) {
        super(context);
    }

    @Override
    protected Response apiCall() throws IOException {
        RatesService ratesService = ApiFactory.getRatesService();
        Call<RateResponse> rateResponseCall = ratesService.getRates();
        RateResponse rateResponse = rateResponseCall.execute().body();
        if (rateResponse!=null) {
            return new Response().setAnswer(rateResponse).setRequestResult(RequestResult.SUCCESS);
        } else {
            return new Response().setAnswer(rateResponse).setRequestResult(RequestResult.ERROR);
        }
    }
}
